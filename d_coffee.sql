--
-- File generated with SQLiteStudio v3.2.1 on พ. ก.ย. 7 23:27:59 2022
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: catagory
DROP TABLE IF EXISTS catagory;

CREATE TABLE catagory (
    category_id   VARCHAR (3) NOT NULL
                              UNIQUE,
    category_name TEXT (50)   NOT NULL,
    PRIMARY KEY (
        category_id
    )
);

INSERT INTO catagory (
                         category_id,
                         category_name
                     )
                     VALUES (
                         'C01',
                         'กาแฟ'
                     );

INSERT INTO catagory (
                         category_id,
                         category_name
                     )
                     VALUES (
                         'C02',
                         'ชา'
                     );

INSERT INTO catagory (
                         category_id,
                         category_name
                     )
                     VALUES (
                         'C03',
                         'น้ำผลไม้'
                     );

INSERT INTO catagory (
                         category_id,
                         category_name
                     )
                     VALUES (
                         'C04',
                         'นม โกโก้ และคาราเมล'
                     );

INSERT INTO catagory (
                         category_id,
                         category_name
                     )
                     VALUES (
                         'C05',
                         'สมูทตี้เพื่อสุขภาพ'
                     );

INSERT INTO catagory (
                         category_id,
                         category_name
                     )
                     VALUES (
                         'C06',
                         'ขนมหวาน'
                     );

INSERT INTO catagory (
                         category_id,
                         category_name
                     )
                     VALUES (
                         'C07',
                         'อาหาร'
                     );


-- Table: customer
DROP TABLE IF EXISTS customer;

CREATE TABLE customer (
    customer_id    VARCHAR (8)  UNIQUE
                                NOT NULL,
    customer_name  TEXT (100)   NOT NULL,
    customer_tel   VARCHAR (10) NOT NULL,
    customer_point INTEGER      NOT NULL,
    PRIMARY KEY (
        customer_id
    )
);

INSERT INTO customer (
                         customer_id,
                         customer_name,
                         customer_tel,
                         customer_point
                     )
                     VALUES (
                         'CM000001',
                         'นางลำไย หวาน',
                         '0998889988',
                         45
                     );

INSERT INTO customer (
                         customer_id,
                         customer_name,
                         customer_tel,
                         customer_point
                     )
                     VALUES (
                         'CM000002',
                         'นางหมูยอ อุบล',
                         '0877745386',
                         9
                     );

INSERT INTO customer (
                         customer_id,
                         customer_name,
                         customer_tel,
                         customer_point
                     )
                     VALUES (
                         'CM000003',
                         'นายแฮม มินตั่น',
                         '0995086978',
                         32
                     );

INSERT INTO customer (
                         customer_id,
                         customer_name,
                         customer_tel,
                         customer_point
                     )
                     VALUES (
                         'CM000004',
                         'นางสร้อย น้อย',
                         '0999999999',
                         400
                     );

INSERT INTO customer (
                         customer_id,
                         customer_name,
                         customer_tel,
                         customer_point
                     )
                     VALUES (
                         'CM000005',
                         'นางสาวน้อย หน่า',
                         '0885004238',
                         25
                     );

INSERT INTO customer (
                         customer_id,
                         customer_name,
                         customer_tel,
                         customer_point
                     )
                     VALUES (
                         'CM000006',
                         'นายน้อย จัย',
                         '0881002493',
                         65
                     );

INSERT INTO customer (
                         customer_id,
                         customer_name,
                         customer_tel,
                         customer_point
                     )
                     VALUES (
                         'CM000007',
                         'นายจัย บาง',
                         '0881002912',
                         12
                     );


-- Table: employee
DROP TABLE IF EXISTS employee;

CREATE TABLE employee (
    employee_id      VARCHAR (8)  NOT NULL
                                  UNIQUE,
    employee_name    TEXT (100)   NOT NULL,
    employee_address TEXT (100)   NOT NULL,
    employee_tel     VARCHAR (10) NOT NULL,
    employee_email   TEXT (100)   NOT NULL,
    PRIMARY KEY (
        employee_id
    )
);

INSERT INTO employee (
                         employee_id,
                         employee_name,
                         employee_address,
                         employee_tel,
                         employee_email
                     )
                     VALUES (
                         'EM000001',
                         'นายบุญ ดินดี',
                         '123 ต.บ้านบึง อ.บ้านบึง จ.ชลบุรี 20170',
                         '0623456789',
                         'boon@gmail.com'
                     );

INSERT INTO employee (
                         employee_id,
                         employee_name,
                         employee_address,
                         employee_tel,
                         employee_email
                     )
                     VALUES (
                         'EM000002',
                         'นางศรี อรทัย',
                         '44 ต.บ้านบึง อ.บ้านบึง จ.ชลบุรี 20170',
                         '0665455555',
                         'zee@gmail.com'
                     );

INSERT INTO employee (
                         employee_id,
                         employee_name,
                         employee_address,
                         employee_tel,
                         employee_email
                     )
                     VALUES (
                         'EM000003',
                         'นายธนดล บุญถึง',
                         '261 ต.แสนสุข อ.เมือง จ.ชลบุรี 20130',
                         '0638844262',
                         'thanadon373@outlook.com'
                     );

INSERT INTO employee (
                         employee_id,
                         employee_name,
                         employee_address,
                         employee_tel,
                         employee_email
                     )
                     VALUES (
                         'EM000004',
                         'นายกอ ขยันดี',
                         '446 ต.บ้านสวน อ.เมือง จ.ชลบุรี 20000',
                         '0644446666',
                         'gg@gmail.com'
                     );

INSERT INTO employee (
                         employee_id,
                         employee_name,
                         employee_address,
                         employee_tel,
                         employee_email
                     )
                     VALUES (
                         'EM000005',
                         'นายขอ สุริน',
                         '1/88 ต.แสนสุข อ.เมือง จ.ชลบุรี 20130',
                         '0894266264',
                         'kk@gmail.com'
                     );

INSERT INTO employee (
                         employee_id,
                         employee_name,
                         employee_address,
                         employee_tel,
                         employee_email
                     )
                     VALUES (
                         'EM000006',
                         'นางสาวคอ ขยันดี',
                         '111 ต.บ้านสวน อ.เมือง จ.ชลบุรี 20000',
                         '0801112233',
                         'ko@gmail.com'
                     );

INSERT INTO employee (
                         employee_id,
                         employee_name,
                         employee_address,
                         employee_tel,
                         employee_email
                     )
                     VALUES (
                         'EM000007',
                         'นางตัวดี สรญา',
                         '33/5 ต.บ้านสวน อ.เมือง จ.ชลบุรี 20000',
                         '0882245286',
                         'dd5@gmail.com'
                     );


-- Table: order
DROP TABLE IF EXISTS [order];

CREATE TABLE [order] (
    order_id       VARCHAR (8) NOT NULL
                               UNIQUE,
    order_queue    INTEGER     NOT NULL,
    order_date     DATE        NOT NULL,
    order_time     TIME        NOT NULL,
    order_discount DOUBLE,
    order_total    DOUBLE      NOT NULL,
    order_recieved DOUBLE      NOT NULL,
    order_change   DOUBLE      NOT NULL,
    order_payment  VARCHAR (5) NOT NULL,
    store_id       VARCHAR (3) NOT NULL,
    employee_id    VARCHAR (8) NOT NULL,
    customer_id    VARCHAR (8),
    PRIMARY KEY (
        order_id
    ),
    FOREIGN KEY (
        store_id
    )
    REFERENCES store ON UPDATE CASCADE,
    FOREIGN KEY (
        employee_id
    )
    REFERENCES employee ON UPDATE CASCADE,
    FOREIGN KEY (
        customer_id
    )
    REFERENCES customer ON UPDATE CASCADE
);

INSERT INTO [order] (
                        order_id,
                        order_queue,
                        order_date,
                        order_time,
                        order_discount,
                        order_total,
                        order_recieved,
                        order_change,
                        order_payment,
                        store_id,
                        employee_id,
                        customer_id
                    )
                    VALUES (
                        'OR000001',
                        1,
                        '26/8/2022',
                        '7:30',
                        0.0,
                        90.0,
                        90.0,
                        0.0,
                        'P',
                        'S01',
                        'EM000003',
                        NULL
                    );

INSERT INTO [order] (
                        order_id,
                        order_queue,
                        order_date,
                        order_time,
                        order_discount,
                        order_total,
                        order_recieved,
                        order_change,
                        order_payment,
                        store_id,
                        employee_id,
                        customer_id
                    )
                    VALUES (
                        'OR000002',
                        2,
                        '26/8/2022',
                        '7:45',
                        5.0,
                        118.0,
                        118.0,
                        0.0,
                        'P',
                        'S01',
                        'EM000003',
                        'CM000004'
                    );

INSERT INTO [order] (
                        order_id,
                        order_queue,
                        order_date,
                        order_time,
                        order_discount,
                        order_total,
                        order_recieved,
                        order_change,
                        order_payment,
                        store_id,
                        employee_id,
                        customer_id
                    )
                    VALUES (
                        'OR000003',
                        3,
                        '26/8/2022',
                        '8:42',
                        0.0,
                        55.0,
                        60.0,
                        5.0,
                        'C',
                        'S01',
                        'EM000003',
                        NULL
                    );

INSERT INTO [order] (
                        order_id,
                        order_queue,
                        order_date,
                        order_time,
                        order_discount,
                        order_total,
                        order_recieved,
                        order_change,
                        order_payment,
                        store_id,
                        employee_id,
                        customer_id
                    )
                    VALUES (
                        'OR000004',
                        4,
                        '26/8/2022',
                        '10:31',
                        5.0,
                        152.0,
                        200.0,
                        48.0,
                        'C',
                        'S01',
                        'EM000004',
                        'CM000003'
                    );


-- Table: order_item
DROP TABLE IF EXISTS order_item;

CREATE TABLE order_item (
    order_item_id     INTEGER     NOT NULL
                                  UNIQUE,
    order_item_amount INTEGER     NOT NULL,
    order_item_price  DOUBLE      NOT NULL,
    order_item_total  DOUBLE      NOT NULL,
    product_id        VARCHAR (6) NOT NULL,
    order_id          VARCHAR (8) NOT NULL,
    PRIMARY KEY (
        order_item_id
    ),
    FOREIGN KEY (
        product_id
    )
    REFERENCES product ON UPDATE CASCADE,
    FOREIGN KEY (
        order_id
    )
    REFERENCES [order] ON UPDATE CASCADE
);

INSERT INTO order_item (
                           order_item_id,
                           order_item_amount,
                           order_item_price,
                           order_item_total,
                           product_id,
                           order_id
                       )
                       VALUES (
                           1,
                           1,
                           55.0,
                           55.0,
                           'P00004',
                           'OR000001'
                       );

INSERT INTO order_item (
                           order_item_id,
                           order_item_amount,
                           order_item_price,
                           order_item_total,
                           product_id,
                           order_id
                       )
                       VALUES (
                           2,
                           1,
                           35.0,
                           35.0,
                           'P00006',
                           'OR000001'
                       );

INSERT INTO order_item (
                           order_item_id,
                           order_item_amount,
                           order_item_price,
                           order_item_total,
                           product_id,
                           order_id
                       )
                       VALUES (
                           3,
                           2,
                           35.0,
                           70.0,
                           'P00003',
                           'OR000002'
                       );

INSERT INTO order_item (
                           order_item_id,
                           order_item_amount,
                           order_item_price,
                           order_item_total,
                           product_id,
                           order_id
                       )
                       VALUES (
                           4,
                           1,
                           55.0,
                           55.0,
                           'P00004',
                           'OR000002'
                       );

INSERT INTO order_item (
                           order_item_id,
                           order_item_amount,
                           order_item_price,
                           order_item_total,
                           product_id,
                           order_id
                       )
                       VALUES (
                           5,
                           1,
                           55.0,
                           55.0,
                           'P00007',
                           'OR000003'
                       );

INSERT INTO order_item (
                           order_item_id,
                           order_item_amount,
                           order_item_price,
                           order_item_total,
                           product_id,
                           order_id
                       )
                       VALUES (
                           6,
                           1,
                           55.0,
                           55.0,
                           'P00005',
                           'OR000004'
                       );

INSERT INTO order_item (
                           order_item_id,
                           order_item_amount,
                           order_item_price,
                           order_item_total,
                           product_id,
                           order_id
                       )
                       VALUES (
                           7,
                           3,
                           35.0,
                           105.0,
                           'P00006',
                           'OR000004'
                       );


-- Table: product
DROP TABLE IF EXISTS product;

CREATE TABLE product (
    product_id    VARCHAR (6) NOT NULL
                              UNIQUE,
    product_name  TEXT (50)   NOT NULL,
    product_type  VARCHAR (5),
    product_size  VARCHAR (5),
    product_price DOUBLE      NOT NULL,
    category_id   VARCHAR (3) NOT NULL,
    PRIMARY KEY (
        product_id
    ),
    FOREIGN KEY (
        category_id
    )
    REFERENCES catagory ON UPDATE CASCADE
);

INSERT INTO product (
                        product_id,
                        product_name,
                        product_type,
                        product_size,
                        product_price,
                        category_id
                    )
                    VALUES (
                        'P00001',
                        'เอสเพรสโซ่',
                        'HCS',
                        'SML',
                        49.0,
                        'C01'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_type,
                        product_size,
                        product_price,
                        category_id
                    )
                    VALUES (
                        'P00002',
                        'ชาเขียว',
                        'CS',
                        'ML',
                        55.0,
                        'C02'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_type,
                        product_size,
                        product_price,
                        category_id
                    )
                    VALUES (
                        'P00003',
                        'คุกกี้ธัญพืช',
                        '-',
                        '-',
                        35.0,
                        'C06'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_type,
                        product_size,
                        product_price,
                        category_id
                    )
                    VALUES (
                        'P00004',
                        'ลาเต้',
                        'CS',
                        'ML',
                        55.0,
                        'C01'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_type,
                        product_size,
                        product_price,
                        category_id
                    )
                    VALUES (
                        'P00005',
                        'น้ำแอปเปิ้ล',
                        'CS',
                        'ML',
                        55.0,
                        'C03'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_type,
                        product_size,
                        product_price,
                        category_id
                    )
                    VALUES (
                        'P00006',
                        'ครัวซองต์',
                        '-',
                        '-',
                        35.0,
                        'C06'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_type,
                        product_size,
                        product_price,
                        category_id
                    )
                    VALUES (
                        'P00007',
                        'อเมริกาโน่',
                        'HC',
                        'SML',
                        55.0,
                        'C01'
                    );


-- Table: store
DROP TABLE IF EXISTS store;

CREATE TABLE store (
    store_id      VARCHAR (3)  NOT NULL
                               UNIQUE,
    store_name    TEXT (50)    NOT NULL,
    store_address TEXT (100)   NOT NULL,
    store_tel     VARCHAR (10) NOT NULL,
    PRIMARY KEY (
        store_id
    )
);

INSERT INTO store (
                      store_id,
                      store_name,
                      store_address,
                      store_tel
                  )
                  VALUES (
                      'S01',
                      'D-Coffee',
                      'อาคารวิทยาศาสตร์การแพทย์ คณะสหเวชศาสตร์ ม.บูรพา',
                      '0953325290'
                  );


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
