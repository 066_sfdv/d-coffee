CREATE TABLE store(
    store_id        VARCHAR(3)     NOT NULL UNIQUE,
    store_name      TEXT(50)       NOT NULL,
    store_address   TEXT(100)      NOT NULL,
    store_tel       VARCHAR(10)    NOT NULL,
    PRIMARY KEY(store_id)
);

CREATE TABLE category(
    category_id        VARCHAR(3)  NOT NULL UNIQUE,
    category_name      TEXT(50)    NOT NULL,
    PRIMARY KEY(category_id)
);

CREATE TABLE product(
    product_id         VARCHAR(6)         NOT NULL UNIQUE,
    product_name       TEXT(50)           NOT NULL,
    product_type       VARCHAR(5),
    product_size       VARCHAR(5),
    product_price      DOUBLE             NOT NULL, 
    category_id        VARCHAR(3)         NOT NULL,
    PRIMARY KEY(product_id),
    FOREIGN KEY(category_id) REFERENCES category ON UPDATE CASCADE
);

CREATE TABLE employee(
    employee_id           VARCHAR(8)      NOT NULL UNIQUE,
    employee_name         TEXT(100)       NOT NULL,
    employee_address      TEXT(100)       NOT NULL,
    employee_tel          VARCHAR(10)     NOT NULL,
    employee_email        TEXT(100)       NOT NULL,
    PRIMARY KEY(employee_id)
);

CREATE TABLE customer(
    customer_id           VARCHAR(8)      NOT NULL UNIQUE,
    customer_name         TEXT(100)       NOT NULL,
    customer_tel          VARCHAR(10)     NOT NULL,
    customer_point        INTEGER         NOT NULL,
    PRIMARY KEY(customer_id)
);

CREATE TABLE 'order'(
    order_id              VARCHAR(8)    NOT NULL UNIQUE,
    order_queue           INTEGER        NOT NULL,
    order_date            DATE           NOT NULL,
    order_time            TIME           NOT NULL,
    order_discount        DOUBLE,
    order_total           DOUBLE         NOT NULL,
    order_recieved        DOUBLE         NOT NULL,
    order_change          DOUBLE         NOT NULL,
    order_payment         VARCHAR(5)     NOT NULL,
    store_id              VARCHAR(3)     NOT NULL,
    employee_id           VARCHAR(8)     NOT NULL,
    customer_id           VARCHAR(8),
    PRIMARY KEY(order_id),
    FOREIGN KEY(store_id) REFERENCES store ON UPDATE CASCADE,
    FOREIGN KEY(employee_id) REFERENCES employee ON UPDATE CASCADE,
    FOREIGN KEY(customer_id) REFERENCES customer ON UPDATE CASCADE
);

CREATE TABLE order_item(
    order_item_id         INTEGER       NOT NULL UNIQUE,  
    order_item_amount     INTEGER       NOT NULL,
    order_item_price      DOUBLE        NOT NULL,
    order_item_total      DOUBLE        NOT NULL, 
    product_id            VARCHAR(6)    NOT NULL,
    order_id              VARCHAR(8)   NOT NULL,
    PRIMARY KEY(order_item_id),
    FOREIGN KEY(product_id) REFERENCES product ON UPDATE CASCADE,
    FOREIGN KEY(order_id) REFERENCES 'order' ON UPDATE CASCADE    
);